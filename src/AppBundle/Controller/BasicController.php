<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Sentence;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class BasicController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $sentence = new Sentence();
        $en_translation = $sentence->translsate('en');
        $en_translation = $sentence->translsate('en');

        $sentence->translate('fr')->setDescription('Hello');

        $em->persist($sentence);
        $sentence->mergeNewTranslations();
        $sentence->translate('en')->getName();
        $em->flush();

        $form = $this->createFormBuilder()
            ->add('ruDesc', TextType::class, ['label' => 'Описание русское'])
            ->add('enDesc', TextType::class, ['label' => 'Описание английское'])
            ->add('save', SubmitType::class, ['label' => 'Сохранить'])
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $sentence = new Sentence();
            $sentence->translate('en')->setDescription($data['enDesc']);
            $sentence->translate('ru')->setDescription($data['ruDesc']);

            $em = $this->getDoctrine()->getManager();
            $em->persist($sentence);

            $sentence->mergeNewTranslations();
            $em->flush();


            $sentences = $this->getDoctrine()->getRepository('AppBundle:Sentence')->findAll();

            return $this->render('@App/Basic/index.html.twig', [
                'form' => $form->createView(),
                'sentences' => $sentences
            ]);

        }
}}
